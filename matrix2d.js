function multiply2DMatrices(matrix1, matrix2) {
  if (validate_matrix(matrix1, matrix2)) {
    var res = [];

    var matrix1_row_length = matrix1.length;
    var matrix2_col_length = matrix2[0].length;

    for (var i = 0; i < matrix1_row_length; i++) {
      // i: matrix1_row_index, j: matrix2_col_index, k: matrix1_col_index
      res[i] = [];
      for (var j = 0; j < matrix2_col_length; j++) {
        var sum = 0;
        for (var k = 0; k < matrix1[0].length; k++) {
          sum += matrix1[i][k] * matrix2[k][j];
        }
        res[i][j] = sum;
      }
    }
    return res;
  } else {
    return false;
  }
}
function validate_matrix(matrix1, matrix2) {
  var matrix1_col_len = matrix1[0].length;
  var matrix2_row_len = matrix2.length;

  if (matrix1_col_len === matrix2_row_len) {
    return true;
  } else {
    return false;
  }
}

var m1 = [
    [1, 2],
    [3, 4],
  ];
  var m2 = [
    [5, 6],
    [7, 8],
  ];
var output = multiply2DMatrices(m1, m2);

console.log(output);
