// To find the factorial for given number
function factorial(num) {
  if (num === 0 || num === 1) {
    return 1;
  }
  for (var i = num - 1; i > 1; i--) {
    num = num * i;
  }
  return num;
}
factorial(5);
console.log(factorial(5));