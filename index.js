var program = 1;
switch (program) {
  case 1:
    const program_1 = require("./class_variables");
    break;
  case 2:
    const program_2 = require("./combination");
    break;
  case 3:
    const program_3 = require("./factorial");
    break;
  case 4:
    const program_4 = require("./functions");
    break;
  case 5:
    const program_5 = require("./functions_variables");
    break;
  case 6:
    const program_6 = require("./leap");
    break;
  case 7:
    const program_7 = require("./multiply");
    break;
  case 8:
    const program_8 = require("./phone");
    break;
  case 9:
    const program_9 = require("./power");
    break;
  case 10:
    const program_10 = require("./prime");
    break;
  case 11:
    const program_11 = require("./repeating");
    break;
  case 12:
    const program_12 = require("./square");
    break;
  case 13:
    const program_13 = require("./variables");
    break;
  case 14:
    const program_14 = require("./grid");
    break;

  case 15:
    const program_15 = require("./matrix2d");
    break;

  case 16:
    const program_16 = require("./matrix3d");
    break;
}
