const price = 1200; //int type

console.log(typeof price);

let rate = 122.5; // float type

console.log(typeof rate);

let wish = "hello"; // string type

console.log(typeof wish);

var test = true; // boolean type

console.log(typeof test);

var check = null; // null type

console.log(typeof check);

let checkname = { firstname: "john", lastname: "sign" }; //object

console.log(typeof checkname);

let result = undefined; // undefined

console.log(typeof result);

var fruits = ["apple", " mango"]; // array

console.log(typeof fruits);

let y = 123e5; //exponential

console.log(typeof y);
