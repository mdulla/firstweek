
function variables(){

	const price = 1200;					//int type

	console.log(price);

	let rate = 122.5;					// float type

	console.log(rate);

	let wish = "hello";					// string type

	console.log(wish);

	var test = true;					// boolean type

	console.log(test);

	var check = null;					// null type

	console.log(check);

	let checkname ={firstname:"john", lastname:"sign"};					//object

	console.log(checkname);

	let result = undefined;					// undefined

	console.log(result);

	var fruits = ['apple',' mango'];					// array

	console.log(fruits);

	let y = 123e5;					//exponential	

	console.log(y);  									

}

variables();














