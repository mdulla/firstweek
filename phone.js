//To check the given number is a valid number or not

function validnumber(num){
  //Regular expression

  let numberformat = /^0?[6-9]\d{9}$/;		//The number must be in this format only.

	if(numberformat.test(num)){

		return "Given number is a valid number";

	}

	else{

		return "Given number is not a valid number";

	}

}
console.log(validnumber(7032740457));