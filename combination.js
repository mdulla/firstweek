//factorial of a number and to find combinations of a numbers
function factorial(num) {
  if (num === 0 || num === 1) {
    return 1;
  }
  for (var i = num - 1; i > 1; i--) {
    num = num * i;
  }
  return num;
}
factorial(5);
console.log(factorial(5));

function combinations(n, r) {
  var ncr = factorial(n) / (factorial(n - r) * factorial(r));
  return ncr;
}
combinations(5, 3);
console.log(combinations(5, 3));
